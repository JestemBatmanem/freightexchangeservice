﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Linq;
using MekalFirstMobileServiceService.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ServiceOrder = MekalFirstMobileServiceService.DataObjects.Order;

namespace ServiceTests.UtilsTests
{
    [TestClass]
    public class OrderUtilsTests
    {
        [TestMethod]
        public void AreOrderCollectionsEqual_SameOrderCollections_ReturnsTrue()
        {
            string id1 = Guid.NewGuid().ToString();
            string id2 = Guid.NewGuid().ToString();

            var orders1 = new List<ServiceOrder>
            {
                new ServiceOrder
                {
                    Id = id1,
                },
                new ServiceOrder
                {
                    Id = id2,
                }
            };

            var orders2 = new List<ServiceOrder>
            {
                new ServiceOrder
                {
                    Id = id2,
                },
                new ServiceOrder
                {
                    Id = id1,
                },
            };
            Assert.IsTrue(OrderUtils.AreOrderCollectionsEqual(orders1, orders2));
        }

        [TestMethod]
        public void AreOrderCollectionsEqual_DifferentOrderCollections_ReturnsFalse()
        {
            var orders1 = new List<ServiceOrder>
            {
                new ServiceOrder
                {
                    Id = Guid.NewGuid().ToString(),
                },
                new ServiceOrder
                {
                    Id = Guid.NewGuid().ToString(),
                }
            };

            var orders2 = new List<ServiceOrder>
            {
                new ServiceOrder
                {
                    Id = Guid.NewGuid().ToString()
                },
                new ServiceOrder
                {
                    Id = Guid.NewGuid().ToString()
                }
            };

            Assert.IsFalse(OrderUtils.AreOrderCollectionsEqual(orders1, orders2));
        }

        [TestMethod]
        public void QueryClosestOrdersWithinRadius_AllOrdersInRange_ReturnsAllOrders()
        {
            var orders = new List<ServiceOrder>
            {
                new ServiceOrder
                {
                    Id = Guid.NewGuid().ToString(),
                    IssueLocation = DbGeography.FromText("POINT(-121.0 40.0)")
                },
                new ServiceOrder
                {
                    Id = Guid.NewGuid().ToString(),
                    IssueLocation = DbGeography.FromText("POINT(-120.0 40.0)")
                },
                new ServiceOrder
                {
                    Id = Guid.NewGuid().ToString(),
                    IssueLocation = DbGeography.FromText("POINT(-120.5 41.0)")
                }
            };
            var driverLocation = DbGeography.FromText("POINT(-120.5 40.5)");

            var result = OrderUtils.QueryClosestOrdersWithinRadius(orders.AsQueryable(),
                driverLocation, 70000.0).ToList();
            Assert.IsTrue(OrderUtils.AreOrderCollectionsEqual(orders, result));
        }

        [TestMethod]
        public void QueryClosestOrdersWithinRadius_AllOrdersOutOfRange_ReturnsNoOrders()
        {
            var orders = new List<ServiceOrder>
            {
                new ServiceOrder
                {
                    Id = Guid.NewGuid().ToString(),
                    IssueLocation = DbGeography.FromText("POINT(-121.0 40.0)")
                },
                new ServiceOrder
                {
                    Id = Guid.NewGuid().ToString(),
                    IssueLocation = DbGeography.FromText("POINT(-120.0 40.0)")
                },
                new ServiceOrder
                {
                    Id = Guid.NewGuid().ToString(),
                    IssueLocation = DbGeography.FromText("POINT(-120.5 41.0)")
                }
            };
            var driverLocation = DbGeography.FromText("POINT(-118.0 40.0)");

            var result = OrderUtils.QueryClosestOrdersWithinRadius(orders.AsQueryable(),
                driverLocation, 111000.0).ToList();
            Assert.IsFalse(result.Any());
        }

        [TestMethod]
        public void QueryClosestOrdersWithinRadius_SomeOrdersOutOfRange_ReturnsCorrectOrders()
        {
            var orders = new List<ServiceOrder>
            {
                new ServiceOrder
                {
                    Id = Guid.NewGuid().ToString(),
                    IssueLocation = DbGeography.FromText("POINT(-121.0 40.0)")
                },
                new ServiceOrder
                {
                    Id = Guid.NewGuid().ToString(),
                    IssueLocation = DbGeography.FromText("POINT(-120.0 40.0)")
                },
                new ServiceOrder
                {
                    Id = Guid.NewGuid().ToString(),
                    IssueLocation = DbGeography.FromText("POINT(-120.5 41.0)")
                }
            };

            var driverLocation = DbGeography.FromText("POINT(-120.42 39.0)");
            var expectedResult = orders.Take(2);
            var result = OrderUtils.QueryClosestOrdersWithinRadius(orders.AsQueryable(),
                driverLocation, 200000.0).ToList();
            Assert.IsTrue(OrderUtils.AreOrderCollectionsEqual(expectedResult, result));
        }
    }
}