﻿using System.Collections.Generic;
using System.Linq;
using MekalFirstMobileServiceService.DataObjects;
using MekalFirstMobileServiceService.DTO;
using MekalFirstMobileServiceService.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ServiceTests.UtilsTests
{
    [TestClass]
    public class RegistrationRequestValidatorTests
    {
        private readonly IQueryable<Account> _accounts;

        public RegistrationRequestValidatorTests()
        {
            _accounts = new List<Account>{new Account{Email = "test@test.com", Username = "test", Role = 1}}.AsQueryable();
        }

        [TestMethod]
        public void ValidateRegistrationRequest_CorrectRegistrationRequestDto_ReturnsOK()
        {
            var resultMsg = RegistrationRequestValidator.ValidateRegistrationRequest(new RegistrationRequestDto
            {
                Email = "test2@test.com",
                Password = "8CharactersAtLeast",
                Role = 1,
                Username = "uniqueUsername"
            },
                _accounts);
            Assert.AreEqual(RegistrationRequestValidator.ValidationSuccessMessage, resultMsg);
        }

        [TestMethod]
        public void ValidateRegistrationRequest_RequestDtoWithTooShortPassword_ReturnsCorrectMessage()
        {
            var resultMsg = RegistrationRequestValidator.ValidateRegistrationRequest(new RegistrationRequestDto
            {
                Email = "test2@test.com",
                Password = "only7!!",
                Role = 1,
                Username = "uniqueUsername"
            },
                _accounts);
            Assert.AreEqual(RegistrationRequestValidator.InvalidPasswordErrorMessage, resultMsg);
        }

        [TestMethod]
        public void ValidateRegistrationRequest_RequestDtoWithEmptyPassword_ReturnsCorrectMessage()
        {
            var resultMsg = RegistrationRequestValidator.ValidateRegistrationRequest(new RegistrationRequestDto
            {
                Email = "test2@test.com",
                Password = "",
                Role = 1,
                Username = "uniqueUsername"
            },
                _accounts);
            Assert.AreEqual(RegistrationRequestValidator.InvalidPasswordErrorMessage, resultMsg);
        }

        [TestMethod]
        public void ValidateRegistrationRequest_RequestDtoWithTooShortUsername_ReturnsCorrectMessage()
        {
            var resultMsg = RegistrationRequestValidator.ValidateRegistrationRequest(new RegistrationRequestDto
            {
                Email = "test2@test.com",
                Password = "8CharactersAtLeast",
                Role = 1,
                Username = "uni"
            },
                _accounts);
            Assert.AreEqual(RegistrationRequestValidator.InvalidUsernameErrorMessage, resultMsg);
        }

        [TestMethod]
        public void ValidateRegistrationRequest_RequestDtoWithUsernameWithNonAlphanumericChars_ReturnsCorrectMessage()
        {
            var resultMsg = RegistrationRequestValidator.ValidateRegistrationRequest(new RegistrationRequestDto
            {
                Email = "test2@test.com",
                Password = "8CharactersAtLeast",
                Role = 1,
                Username = "user$$$"
            },
                _accounts);
            Assert.AreEqual(RegistrationRequestValidator.InvalidUsernameErrorMessage, resultMsg);
        }

        [TestMethod]
        public void ValidateRegistrationRequest_RequestDtoWithAdminRole_ReturnsCorrectMessage()
        {
            var resultMsg = RegistrationRequestValidator.ValidateRegistrationRequest(new RegistrationRequestDto
            {
                Email = "test2@test.com",
                Password = "8CharactersAtLeast",
                Role = 2,
                Username = "test"
            },
                _accounts);
            Assert.AreEqual(RegistrationRequestValidator.ErrorGivenUserRoleDoesNotExist, resultMsg);
        }

        [TestMethod]
        public void ValidateRegistrationRequest_RequestDtoWithExistingUsername_ReturnsCorrectMessage()
        {
            var resultMsg = RegistrationRequestValidator.ValidateRegistrationRequest(new RegistrationRequestDto
            {
                Email = "test2@test.com",
                Password = "8CharactersAtLeast",
                Role = 1,
                Username = "test"
            },
                _accounts);
            Assert.AreEqual(RegistrationRequestValidator.UsernameAlreadyExistErrorMessage, resultMsg);
        }

        [TestMethod]
        public void ValidateRegistrationRequest_RequestDtoWithInvalidUserEmail_ReturnsCorrectMessage()
        {
            var resultMsg = RegistrationRequestValidator.ValidateRegistrationRequest(new RegistrationRequestDto
            {
                Email = "test@testcom",
                Password = "8CharactersAtLeast",
                Role = 0,
                Username = "uniqueUsername"
            },
                _accounts);
            Assert.AreEqual(RegistrationRequestValidator.InvalidEmailErrorMessage, resultMsg);
        }

        [TestMethod]
        public void ValidateRegistrationRequest_RequestDtoWithExistingUserEmail_ReturnsCorrectMessage()
        {
            var resultMsg = RegistrationRequestValidator.ValidateRegistrationRequest(new RegistrationRequestDto
            {
                Email = "test@test.com",
                Password = "8CharactersAtLeast",
                Role = 0,
                Username = "uniqueUsername"
            },
                _accounts);
            Assert.AreEqual(RegistrationRequestValidator.EmailAlreadyExistErrorMessage, resultMsg);
        }

    }
}