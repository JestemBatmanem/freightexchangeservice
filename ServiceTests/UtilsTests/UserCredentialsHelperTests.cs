﻿using System.Collections.ObjectModel;
using System.Reflection;
using System.Web.Http;
using MekalFirstMobileServiceService.Access;
using MekalFirstMobileServiceService.Utils;
using Microsoft.ServiceBus;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.WindowsAzure.Mobile.Service.Security;
using ServiceTests.TestHelpers;

namespace ServiceTests.UtilsTests
{
    [TestClass]
    public class UserCredentialsHelperTests
    {
        private ApiController _controllerUsedByDriver;
        private ApiController _controllerUsedByCustomer;
        private ApiController _controllerUsedByAdmin;

        public UserCredentialsHelperTests()
        {
            _controllerUsedByDriver = InitializeController(ServiceRoles.Driver);
            _controllerUsedByCustomer = InitializeController(ServiceRoles.Customer);
            _controllerUsedByAdmin = InitializeController(ServiceRoles.Admin);
        }

        [TestMethod]
        [ExpectedException(typeof(ServerErrorException))]
        public void GetCurrentUsername_WithNullUser_ThrowsServerErrorExeption()
        {
            UserCredentialsHelper.GetCurrentUserName(new FakeController());
        }

        [TestMethod]
        public void GetCurrentUsername_WithProviderAndUser_ReturnsUsernameWithoutProvider()
        {
            var result = UserCredentialsHelper.GetCurrentUserName(new FakeController {User = new ServiceUser{Id = "provider:user"}});
            Assert.AreEqual("user", result);
        }

        [TestMethod]
        public void CreateFullUsernameWithProvider_ReturnsCorrectFullUsernameWithCustomProvider()
        {
            var result = UserCredentialsHelper.CreateFullUsernameWithProvider("user");
            Assert.AreEqual("Custom:user",result);
        }

        [TestMethod]
        public void IsControllerUserInRole_ControllerUsedByDriverCheckForDriver_ReturnsTrue()
        {
            Assert.IsTrue(UserCredentialsHelper.IsControllerUserInRole(_controllerUsedByDriver, ServiceRoles.Driver));
        }

        [TestMethod]
        public void IsControllerUserInRole_ControllerUsedByDriverCheckForCustomer_ReturnsFalse()
        {
            Assert.IsFalse(UserCredentialsHelper.IsControllerUserInRole(_controllerUsedByDriver, ServiceRoles.Customer));
        }

        [TestMethod]
        public void IsControllerUserInRole_ControllerUsedByCustomerCheckForCustomer_ReturnsTrue()
        {
            Assert.IsTrue(UserCredentialsHelper.IsControllerUserInRole(_controllerUsedByCustomer, ServiceRoles.Customer));
        }

        [TestMethod]
        public void IsControllerUserInRole_ControllerUsedByAdminCheckForDriver_ReturnsFalse()
        {
            Assert.IsFalse(UserCredentialsHelper.IsControllerUserInRole(_controllerUsedByAdmin, ServiceRoles.Driver));
        }


        private FakeController InitializeController(ServiceRoles role)
        {
            var user = new ServiceUser {Level = AuthorizationLevel.User};
            typeof(ServiceUser).GetProperty("ProviderIdentities", BindingFlags.Instance | BindingFlags.NonPublic)
                .SetValue(user, new Collection<ProviderCredentials>{new CustomLoginProviderCredentials{Role = role}});
            return new FakeController
            {
                User = user
            };
        }

    }
}