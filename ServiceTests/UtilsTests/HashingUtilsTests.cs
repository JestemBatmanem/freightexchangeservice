﻿using MekalFirstMobileServiceService.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ServiceTests.UtilsTests
{
    [TestClass]
    public class HashingUtilsTests
    {
        [TestMethod]
        public void SlowEquals_DifferentByteArrays_ReturnsFalse()
        {
            var a1 = new byte[] {1, 2, 3};
            var b1 = new byte[] {1, 2, 4};
            var a2 = new byte[] {2, 2, 2};
            var b2 = new byte[] {2, 2, 2, 2};
            var a3 = new byte[] {2, 1, 2, 2};
            var b3 = new byte[] {1, 2, 2, 2};

            Assert.IsFalse(HashingUtils.SlowEquals(a1, b1));
            Assert.IsFalse(HashingUtils.SlowEquals(a2, b2));
            Assert.IsFalse(HashingUtils.SlowEquals(a3, b3));
        }

        [TestMethod]
        public void SlowEquals_SameByteArrays_ReturnsTrue()
        {
            var a1 = new byte[] {1, 2, 3};
            var b1 = new byte[] {1, 2, 3};
            var a2 = new byte[] {2, 2, 2};
            var b2 = new byte[] {2, 2, 2};
            Assert.IsTrue(HashingUtils.SlowEquals(a1, b1));
            Assert.IsTrue(HashingUtils.SlowEquals(a2, b2));
        }

        [TestMethod]
        public void GenerateSalt_Twice_ReturnsDifferentSalt()
        {
            Assert.IsFalse(HashingUtils.SlowEquals(HashingUtils.GenerateSalt(),
                HashingUtils.GenerateSalt()));
            Assert.IsFalse(HashingUtils.SlowEquals(HashingUtils.GenerateSalt(),
                HashingUtils.GenerateSalt()));
        }

        [TestMethod]
        public void Hash_DifferentSaltSamePassword_ReturnsDifferentByteArray()
        {
            byte[] salt1 = HashingUtils.GenerateSalt();
            var salt2 = salt1.Clone() as byte[];
            salt2[salt2.Length - 1] =
                (byte)
                    (salt2[salt2.Length - 1] + 1 > byte.MaxValue
                        ? salt2[salt2.Length - 1] - 1
                        : salt2[salt2.Length - 1] + 1);

            string password = "My password is so GREAT!@#";
            Assert.IsFalse(HashingUtils.SlowEquals(HashingUtils.Hash(password, salt1),
                HashingUtils.Hash(password, salt2)));
        }

        [TestMethod]
        public void Hash_DifferentPasswordSameSalt_ReturnsDifferentByteArray()
        {
            byte[] salt = HashingUtils.GenerateSalt();
            string password1 = "My password is so GREAT!@#";
            string password2 = "My password is not so GREAT!@#";
            Assert.IsFalse(HashingUtils.SlowEquals(HashingUtils.Hash(password1, salt),
                HashingUtils.Hash(password2, salt)));
        }

        [TestMethod]
        public void Hash_SameSaltAndPassword_ReturnsTheSameByteArray()
        {
            byte[] salt = HashingUtils.GenerateSalt();
            string password = "My password is so GREAT!@#";
            Assert.IsTrue(HashingUtils.SlowEquals(HashingUtils.Hash(password, salt),
                HashingUtils.Hash(password, salt)));
        }
    }
}