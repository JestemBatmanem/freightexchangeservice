﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ServiceTests.IntegrationTests.DomainEntities
{
    [Table("MekalFirstMobileService.Accounts")]
    public class Account
    {
        public string Id { get; set; }
        public byte Role { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public byte[] Salt { get; set; }
        public byte[] SaltedAndHashedPassword { get; set; }
        public bool Deleted { get; set; }
    }
}