﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ServiceTests.IntegrationTests.DomainEntities
{
    [Table("MekalFirstMobileService.Conversations")]
    public class Conversation
    {
        public string Id { get; set; }
        public string Username1 { get; set; }
        public string Username2 { get; set; }
        public DateTime LastMessageSentTime { get; set; }
        public string LastMessageText { get; set; }
        public virtual ICollection<Message> Messages { get; set; }
    }
}
