﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ServiceTests.IntegrationTests.DomainEntities
{
    [Table("MekalFirstMobileService.Messages")]
    public class Message
    {
        public string Id { get; set; }
        public string Sender { get; set; }
        public string Text { get; set; }
        public DateTime TimeSent { get; set; }
        public virtual Conversation Conversation { get; set; }
    }
}
