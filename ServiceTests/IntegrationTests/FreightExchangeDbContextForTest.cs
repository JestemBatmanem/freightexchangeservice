﻿using System.Data.Entity;
using ServiceTests.IntegrationTests.DomainEntities;

namespace ServiceTests.IntegrationTests
{
    class FreightExchangeDbContextForTest : DbContext
    {
        public FreightExchangeDbContextForTest(string connectionString) : base(connectionString)
        {
        }
        public IDbSet<Account> Accounts { get; set; }
        public IDbSet<Conversation> Conversations { get; set; }
    }
}
