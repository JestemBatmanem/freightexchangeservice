﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.WindowsAzure.MobileServices;
using Newtonsoft.Json.Linq;
using ServiceTests.IntegrationTests.DomainEntities;
using ServiceTests.TestHelpers;

namespace ServiceTests.IntegrationTests
{
    [TestClass]
    public class RegistrationTests
    {
        //change app.config for testing the deployed service 
        private static readonly MobileServiceClient MobileServiceClient = new MobileServiceClient(
            ConfigurationManager.AppSettings.Get("serviceUrl"),
            ConfigurationManager.AppSettings.Get("serviceApplicationKey")
        );

        private static Account _testAccount;
        private static Account _newAccount = new Account
        {
            Role = 0,
            Email = "test2@test.com",
            Username = "test2"
        };

        [ClassInitialize]
        public static void Init(TestContext context)
        {
            _testAccount = new Account
            {
                Id = Guid.NewGuid().ToString(),
                Username = "test",
                Role = 0,
                Email = "test@test.com",
                Deleted = false,
                Salt =
                    ZumoTokenGenerator.StringToByteArray(
                        "3A5FE8B6F2D08E142821FB38FEF8BDDBB9385C9C5CFE6EC32DF812BB48B90E13F6CA187CF4F178D72ED4244963EDFAF90B2F5E63156780174FE20693BA1ED850AA733292120A1F286EB140ABA26F71B624749AD4B7ABBD8FC54A58A7B259128664662523753FE92BFEA84A7FF5C676156BD3330ADC297491A3533CEAD3C089E36319747BD0DD5FCA94ECF509E60736B44BE4858D88BB9092CF31A9FDD5DF90214783F58D450EBEAE425D0D159852B858624FBB8ED1298257D7A4164D9381942E0A45D08F37BEA779FA1D6545FE3800EAC4B75976DA04203F9D52F6E569E0CB224C0F98C54C6B78C872D5F7DDDE1C89536584640F040715E5BDD090E9D242EEB8"),
                SaltedAndHashedPassword =
                    ZumoTokenGenerator.StringToByteArray(
                        "8757143A426C51A7C026C37809AD51FC74C88FCF7CF771E3021937F59B9E70B82605F5BC332F9CEEC9CF79CBB8FCB9EF3BF3EE461A422E1FBCD8DDAEE9E47ABD")
            };
            using (var db = new FreightExchangeDbContextForTest(ConfigurationManager.AppSettings.Get("MS_TableConnectionString")))
            {
                db.Accounts.Add(_testAccount);
                db.SaveChanges();
            }
        }

        [ClassCleanup]
        public static void CleanUp()
        {
            using (
                var db = new FreightExchangeDbContextForTest(ConfigurationManager.AppSettings.Get("MS_TableConnectionString")))
            {
                var accountsToDelete = db.Accounts.Where(a => a.Username == _testAccount.Username || a.Username == _newAccount.Username);
                foreach (var account in accountsToDelete) db.Accounts.Remove(account);
                db.SaveChanges();
            }
        }
        [TestMethod]
        public async Task Register_CorrectCredentials_ChangesDBAndReturnsHTTPCreated()
        {
            var registrationRequestDto = CreateJObjectForRegistration(_newAccount.Role, _newAccount.Email, _newAccount.Username, "testtest");
            HttpContent content = new ObjectContent(typeof(JObject), registrationRequestDto, new JsonMediaTypeFormatter());
            var response = await MobileServiceClient.InvokeApiAsync("/register", content, HttpMethod.Post, null, null);

            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
    
            List<Account> newAccount;
            using (
                var db = new FreightExchangeDbContextForTest(ConfigurationManager.AppSettings.Get("MS_TableConnectionString")))
            {
                newAccount = db.Accounts.Where(a => a.Username == _newAccount.Username).ToList();
            }
            Assert.AreEqual(1, newAccount.Count);
            Assert.AreEqual(_newAccount.Email, newAccount.First().Email);
            Assert.AreEqual(_newAccount.Role, newAccount.First().Role);

        }

        [TestMethod]
        public async Task Register_UsernameAlreadyTaken_NotChangeDBAndReturnsHTTPBadRequest()
        {
            //Use HttpClient for MobileServiceClient class wraps 404 in an exception and can't read the message from that 
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(ConfigurationManager.AppSettings.Get("serviceUrl"));
                var response = await client.PostAsJsonAsync("/register", new
                {
                    _newAccount.Role,
                    Email = "somecrazymail@mail.com", 
                    _newAccount.Username, 
                    Password = "testtest"
                });
                var responseString = await response.Content.ReadAsStringAsync();
                Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
                Assert.AreEqual("{\"message\":\"" + "Username already exists" + "\"}", responseString);
            }

            List<Account> newAccount;
            using (
                var db = new FreightExchangeDbContextForTest(ConfigurationManager.AppSettings.Get("MS_TableConnectionString")))
            {
                newAccount = db.Accounts.Where(a => a.Username == _newAccount.Username).ToList();
            }
            Assert.IsFalse(newAccount.Any());

        }

        private JObject CreateJObjectForRegistration(byte role, string email, string username, string password)
        {
            return new JObject {{"role", role}, {"email", email}, { "username", username }, { "password", password} };
        }
    }
}
