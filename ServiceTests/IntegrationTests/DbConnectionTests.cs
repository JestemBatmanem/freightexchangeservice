﻿using System.Configuration;
using System.Data.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ServiceTests.IntegrationTests
{
    [TestClass]
    public class DbConnectionTests
    {
        [TestMethod]
        public void MSTableConnectionString_CheckDatabaseConnection()
        {
            using (var db = new DataContext(ConfigurationManager.AppSettings.Get("MS_TableConnectionString")))
            {
                Assert.IsTrue(db.DatabaseExists());
            }
        }
    }
}
