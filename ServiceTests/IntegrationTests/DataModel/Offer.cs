﻿namespace ServiceTests.IntegrationTests.DataModel
{
    public class Offer
    {
        public string Id { get; set; }

        public string Description { get; set; }

    }
}
