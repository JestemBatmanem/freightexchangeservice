﻿using System;

namespace ServiceTests.IntegrationTests.DataModel
{
    public class Order
    {
        public string Id { get; set; }

        public string CargoIssueLocation { get; set; }

        public string CargoDeliveryDestination { get; set; }

        public DateTime IssueDate { get; set; }

        public DateTime DeliveryDate { get; set; }

    }
}
