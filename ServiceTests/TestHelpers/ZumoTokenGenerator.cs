﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace ServiceTests.TestHelpers
{
    internal class ZumoTokenGenerator
    {
        public static string CreateZumoToken(string masterSecret, string userId, DateTime notBefore, DateTime expiry,
            byte role)
        {
            int nNotBefore = convertToUnixTimestamp(notBefore);
            int nExpiry = convertToUnixTimestamp(expiry);

            string jwtHeader = "{\"typ\":\"JWT\",\"alg\":\"HS256\"}";
            string jwtPayload =
                "{\"iss\":\"urn:microsoft:windows-azure:zumo\",\"aud\":\"urn:microsoft:windows-azure:zumo\",\"nbf\":" +
                nNotBefore + ",\"exp\":" + nExpiry + ",\"urn:microsoft:credentials\":\"{" + "\"" + "role\":" + role +
                "}\",\"uid\":\"" + userId + "\",\"ver\":\"2\"}";

            string encodedHeader = urlFriendly(EncodeTo64(jwtHeader));
            string encodedPayload = urlFriendly(EncodeTo64(jwtPayload));

            string stringToSign = encodedHeader + "." + encodedPayload;
            byte[] bytesToSign = Encoding.UTF8.GetBytes(stringToSign);

            string keyJWTSig = masterSecret + "JWTSig";
            byte[] keyBytes = Encoding.Default.GetBytes(keyJWTSig);

            var hash = new SHA256Managed();
            byte[] signingBytes = hash.ComputeHash(keyBytes);

            var sha = new HMACSHA256(signingBytes);
            byte[] signature = sha.ComputeHash(bytesToSign);

            string encodedPart3 = urlFriendly(EncodeTo64(signature));

            return string.Format("{0}.{1}.{2}", encodedHeader, encodedPayload, encodedPart3);
        }

        public static string EncodeTo64(string toEncode)
        {
            byte[] toEncodeAsBytes
                = Encoding.ASCII.GetBytes(toEncode);

            return EncodeTo64(toEncodeAsBytes);
        }

        public static string EncodeTo64(byte[] toEncodeAsBytes)
        {
            string returnValue
                = Convert.ToBase64String(toEncodeAsBytes);
            return returnValue;
        }

        public static DateTime ConvertFromUnixTimestamp(double timestamp)
        {
            var origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return origin.AddSeconds(timestamp);
        }

        public static int convertToUnixTimestamp(DateTime dateTime)
        {
            var origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return (int) Math.Round((dateTime - origin).TotalSeconds, 0);
        }

        private static string urlFriendly(string input)
        {
            input = input.Replace('+', '-');
            input = input.Replace('/', '_');
            input = input.Replace("=", string.Empty);
            return input;
        }

        public static byte[] StringToByteArray(String hex)
        {
            int numberChars = hex.Length;
            byte[] bytes = new byte[numberChars / 2];
            for (int i = 0; i < numberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }
    }
}