﻿using System.Collections.Generic;
using System.Linq;
using MekalFirstMobileServiceService.DataObjects;
using MekalFirstMobileServiceService.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MekalFirstMobileServiceService.Chat;

namespace ServiceTests.ChatTests
{
    [TestClass]
    public class ChatTests
    {
        private FakeConversationManager _manager;
        private Chat _chat;
        [TestInitialize]
        public void Init()
        {
            _manager = new FakeConversationManager();
            _manager.Conversations = new List<Conversation>();
            _chat = new Chat(_manager);
        }

        [TestMethod]
        public void SendMessage_TheFirstTime_CreatesNewConversationWithOneMessage()
        {
            _chat.SendMessage("username1", "username2", "text");

            Assert.AreEqual(1, _chat.ActiveConversations.Count);
            var newConversation = _chat.ActiveConversations.First();
            Assert.IsTrue("username1" == newConversation.Username1 || "username1" == newConversation.Username2);
            Assert.AreEqual("text", newConversation.LastMessageText);            
            var newMessage = newConversation.Messages.First();
            Assert.AreEqual("username1", newMessage.Sender);
            Assert.AreEqual("text", newMessage.Text);
            Assert.AreEqual(newConversation.LastMessageSentTime, newMessage.TimeSent);
        }

        [TestMethod]
        public void SendMessage_TheSecondTime_AddsMessageToExistingConversation()
        {
            _chat.SendMessage("username1", "username2", "hello");
            _chat.SendMessage("username2", "username1", "hello too");

            Assert.AreEqual(1, _chat.ActiveConversations.Count);
            var conversation = _chat.ActiveConversations.First();
            var messages = conversation.Messages;            
            Assert.AreEqual(2, messages.Count);
            Assert.AreEqual(conversation.LastMessageSentTime, messages.First(m => m.Text == "hello too").TimeSent);
        }

        [TestMethod]
        public void SendMessage_ManyMassagesWithDifferentUsers_CreatesManyConversations()
        {
            _chat.SendMessage("username1", "username2", "hello");
            _chat.SendMessage("username1", "username2", "hello");
            _chat.SendMessage("username3", "username4", "hello");
            _chat.SendMessage("username2", "username3", "hello");
            Assert.AreEqual(3, _chat.ActiveConversations.Count);
        }

        [TestMethod]
        public void ActiveConversations_AfterApplicationRestart_IsEmpty()
        {
            _chat.SendMessage("username1", "username2", "hello");
            _chat.SendMessage("username1", "username3", "hello");
            _chat = new Chat(_manager);
            Assert.AreEqual(0, _chat.ActiveConversations.Count);
        }

        [TestMethod]
        public void SendMessage_ToExistingConversationAfterApplicationRestart_AddsMessageToExistingConversation()
        {
            _chat.SendMessage("username1", "username2", "hello");
            _chat.Dispose();
            _chat = new Chat(_manager);
            _chat.SendMessage("username2", "username1", "hello too");

            Assert.AreEqual(1, _chat.ActiveConversations.Count);
            var conversation = _chat.ActiveConversations.First();
            var messages = conversation.Messages;
            Assert.AreEqual(2, messages.Count);
            Assert.AreEqual(conversation.LastMessageSentTime, messages.First(m => m.Text == "hello too").TimeSent);
        }

        [TestMethod]
        public void ChatWithOneMessage_LogOutUser_EmptiesActiveConversations()
        {
            _chat.SendMessage("username1", "username2", "hello");
            _chat.LogOut("username1");
            Assert.IsFalse(_chat.ActiveConversations.Any());
        }

        [TestMethod]
        public void ChatWithOneMessage_LogOutUserAndSendAnotherMessage_MakeOneActiveConversationsWithTwoMessages()
        {
            _chat.SendMessage("username1", "username2", "hello");
            _chat.LogOut("username1");
            _chat.SendMessage("username1", "username2", "hello");
            Assert.AreEqual(1, _chat.ActiveConversations.Count);
            Assert.AreEqual(2, _chat.ActiveConversations.First().Messages.Count);
        }

        [TestMethod]
        public void ChatWithConversationWithMesssagesFromTwoUsers_LogOutSingleUser_DoNotChangeActiveConversations()
        {
            _chat.ActiveUsers.Add("username1");
            _chat.ActiveUsers.Add("username2");
            _chat.SendMessage("username1", "username2", "hello");
            _chat.SendMessage("username2", "username1", "hi");
            _chat.SendMessage("username2", "username1", "how are you?");
            _chat.LogOut("username1");
            Assert.AreEqual(1, _chat.ActiveConversations.Count);
            Assert.AreEqual(3, _chat.ActiveConversations.First().Messages.Count);
        }

        [TestMethod]
        public void ChatWithConversationWithMesssagesFromTwoUsers_LogOutBothUsers_EmptiesActiveConversations()
        {
            _chat.SendMessage("username1", "username2", "hello");
            _chat.SendMessage("username2", "username1", "hi");
            _chat.SendMessage("username2", "username1", "how are you?");
            _chat.LogOut("username1");
            _chat.LogOut("username2");
            Assert.AreEqual(0, _chat.ActiveConversations.Count);
        }

        [TestMethod]
        public void ChatWithConversationWithMesssagesFromTwoUsers_LogOutBothUsersAndSendAnotherMessage_RetrievesAllArchivedMessages()
        {
            _chat.SendMessage("username1", "username2", "hello");
            _chat.SendMessage("username2", "username1", "hi");
            _chat.SendMessage("username2", "username1", "how are you?");
            _chat.LogOut("username1");
            _chat.LogOut("username2");
            _chat.SendMessage("username1", "username2", "I'm fine, thanks");
            Assert.AreEqual(1, _chat.ActiveConversations.Count);
            Assert.AreEqual(4, _chat.ActiveConversations.First().Messages.Count);
            Assert.AreEqual(4, _chat.ActiveConversations.First().Messages.Count);
            Assert.IsTrue(_chat.ActiveConversations.First().Messages.Any(m => m.Sender == "username1" && m.Text == "hello"));
            Assert.IsTrue(_chat.ActiveConversations.First().Messages.Any(m => m.Sender == "username2" && m.Text == "how are you?"));

        }

        [TestMethod]
        public void LogIn_AddsUserToActiveUsers()
        {
            const string username = "test";
            _chat.LogIn(username);
            Assert.IsTrue(_chat.ActiveUsers.Contains(username));
        }

    }

    internal class FakeConversationManager : IConversationManager
    {
        public IFreightExchangeDbContext Context { get; private set; }
        public List<Conversation> Conversations { get; set; }
        public Conversation GetOrDefault(string username1, string username2)
        {
            return
                Conversations.FirstOrDefault(
                    c => (c.Username1.Equals(username1) && c.Username2.Equals(username2)) ||
                         (c.Username1.Equals(username2) && c.Username2.Equals(username1)));
        }

        public IEnumerable<Conversation> GetAllWithUser(string username)
        {
            return Conversations.Where(c => c.Username1 == username || c.Username2 == username).ToList();
        }

        public void Update(Conversation conversation)
        {
            var conversationToUpdate = Conversations.SingleOrDefault(c => c.Id == conversation.Id);
            if (conversationToUpdate != null)
            {
                conversationToUpdate.LastMessageSentTime = conversation.LastMessageSentTime;
                conversationToUpdate.LastMessageText = conversation.LastMessageText;
                AddNewMessagesToExistingConversation(conversationToUpdate, conversation);
            }
            else Conversations.Add(conversation);
        }

        private static void AddNewMessagesToExistingConversation(Conversation source, Conversation target)
        {
            IEnumerable<Message> newMessages =
                source.Messages.Where(m => m.TimeSent > target.Messages.Max(n => n.TimeSent));
            foreach (Message message in newMessages)
            {
                target.Messages.Add(message);
            }
        }

        public void SaveChanges()
        {
        }
    }
}
