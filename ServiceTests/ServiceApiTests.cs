﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.WindowsAzure.MobileServices;
using System.Configuration;
using ServiceTests.IntegrationTests.DataModel;

namespace ServiceTests
{
    [TestClass]
    public class ServiceApiTests
    {
        //change app.config for testing the deployed service 
        private static readonly MobileServiceClient MobileService = new MobileServiceClient(
            ConfigurationManager.AppSettings.Get("serviceUrl"),
            ConfigurationManager.AppSettings.Get("serviceApplicationKey") 
        );
        [TestMethod]
        public async Task OrderCRUDTest()
        {
            IMobileServiceTable<Order> orderTable = MobileService.GetTable<Order>();
            MobileServiceCollection<Order, Order> orders = await orderTable.ToCollectionAsync();
            CollectionAssert.AreEquivalent(orders, await orderTable.ToCollectionAsync());
            var orderToInsert = new Order
            {
                CargoDeliveryDestination = "A",
                CargoIssueLocation = "B",
                DeliveryDate = new DateTime(2015, 5, 5),
                IssueDate = new DateTime(2015, 5, 4)
            };
            await orderTable.InsertAsync(orderToInsert);
            CollectionAssert.AreNotEquivalent(orders, await orderTable.ToCollectionAsync());
            var orderWithId = orderTable.Select(o => o.CargoDeliveryDestination == orderToInsert.CargoDeliveryDestination && o.CargoIssueLocation == orderToInsert.CargoIssueLocation).Query.First();


            var offer = new Offer
            {
                Description = "Test",
            };
        }
    }
}
