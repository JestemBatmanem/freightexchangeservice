﻿using System;
using Microsoft.WindowsAzure.Mobile.Service;

namespace MekalFirstMobileServiceService.DTO
{
    public class OrderDto : EntityData
    {
        public string CargoIssueAddressCity { get; set; }
        public string CargoIssueAddressStreet { get; set; }
        public short CargoIssueAddressStreetNumber { get; set; }
        public short CargoIssueAddressApartmentNumber { get; set; }
        public DateTime IssueDate { get; set; }
        public double IssueLocationLatitude { get; set; }
        public double IssueLocationLongitude { get; set; }


        public string CargoDeliveryAddressCity { get; set; }
        public string CargoDeliveryAddressStreet { get; set; }
        public short CargoDeliveryAddressStreetNumber { get; set; }
        public short CargoDeliveryAddressApartmentNumber { get; set; }
        public DateTime DeliveryDate { get; set; }
        public double DeliveryLocationLatitude { get; set; }
        public double DeliveryLocationLongitude { get; set; }
    }
}