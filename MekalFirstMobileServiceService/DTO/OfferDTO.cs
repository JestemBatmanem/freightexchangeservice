﻿using Microsoft.WindowsAzure.Mobile.Service;

namespace MekalFirstMobileServiceService.DTO
{
    public class OfferDto : EntityData
    {
        public string Description { get; set; }
        public decimal Value { get; set; }
        public string Currency { get; set; }
        public string OrderId { get; set; }
        public string MeanOfTransportId { get; set; }
    }
}