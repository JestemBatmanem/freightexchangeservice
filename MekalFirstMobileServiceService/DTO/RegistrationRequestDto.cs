﻿namespace MekalFirstMobileServiceService.DTO
{
    public class RegistrationRequestDto
    {
        public byte Role { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}