﻿namespace MekalFirstMobileServiceService.DTO
{
    public class RegisterPushDto
    {
        public string Token { get; set; }
        public string Vendor { get; set; }
    }
}