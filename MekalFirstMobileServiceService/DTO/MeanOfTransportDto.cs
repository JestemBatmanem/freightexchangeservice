﻿namespace MekalFirstMobileServiceService.DTO
{
    public class MeanOfTransportDto
    {
        public float Width { get; set; }
        public float Height { get; set; }
        public float Length { get; set; }
        public int Weight { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}