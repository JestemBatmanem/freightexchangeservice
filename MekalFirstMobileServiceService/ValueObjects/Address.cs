﻿using System;

namespace MekalFirstMobileServiceService.ValueObjects
{
    public class Address : IEquatable<Address>
    {
        public string City { get; set; }
        public string Street { get; set; }
        public short StreetNumber { get; set; }
        public short ApartmentNumber { get; set; }
        
        public bool Equals(Address other)
        {
            if (ReferenceEquals(other, null)) return false;
            if (ReferenceEquals(this, other)) return true;
            return City.Equals(other.City) && Street.Equals(other.Street) && StreetNumber.Equals(other.StreetNumber) &&
                   ApartmentNumber.Equals(other.ApartmentNumber);
        }
    }
}