﻿using System.Collections.Generic;
using System.Linq;
using MekalFirstMobileServiceService.DataObjects;

namespace MekalFirstMobileServiceService.Models
{
    public class ConversationManager : IConversationManager
    {
        public ConversationManager(IFreightExchangeDbContext context)
        {
            Context = context;
        }

        public IFreightExchangeDbContext Context { get; private set; }

        public Conversation GetOrDefault(string username1, string username2)
        {
            return
                Context.Conversations.FirstOrDefault(
                    c => (c.Username1.Equals(username1) && c.Username2.Equals(username2)) ||
                         (c.Username1.Equals(username2) && c.Username2.Equals(username1)));

        }

        public IEnumerable<Conversation> GetAllWithUser(string username)
        {
            return Context.Conversations.Where(c => c.Username1 == username || c.Username2 == username).ToList();
        }

        public void Update(Conversation conversation)
        {
            var conversationToUpdate = Context.Conversations.SingleOrDefault(c => c.Id == conversation.Id);
            if (conversationToUpdate != null)
            {
                conversationToUpdate.LastMessageSentTime = conversation.LastMessageSentTime;
                conversationToUpdate.LastMessageText = conversation.LastMessageText;
                AddNewMessagesToExistingConversation(conversationToUpdate, conversation);
            }
            else Context.Conversations.Add(conversation);
        }

        public void SaveChanges()
        {
            Context.SaveChanges();
        }

        private static void AddNewMessagesToExistingConversation(Conversation source, Conversation target)
        {
            IEnumerable<Message> newMessages =
                source.Messages.Where(m => m.TimeSent > target.Messages.Max(n => n.TimeSent));
            foreach (Message message in newMessages)
            {
                target.Messages.Add(message);
            }
        }
    }
}