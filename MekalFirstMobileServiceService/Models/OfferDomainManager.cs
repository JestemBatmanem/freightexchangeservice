﻿using System.Linq;
using System.Net.Http;
using AutoMapper;
using MekalFirstMobileServiceService.DataObjects;
using MekalFirstMobileServiceService.DTO;
using Microsoft.WindowsAzure.Mobile.Service;

namespace MekalFirstMobileServiceService.Models
{
    internal class OfferDomainManager : ComplexTypeDomainManager<OfferDto, Offer, string>
    {
        public OfferDomainManager(HttpRequestMessage request,
            ApiServices services) : base(new FreightExchangeDbContext(), request, services)
        {
        }

        public IQueryable<OfferDto> GetDriverOffers(string driverUsername)
        {
            var offerQuery = Context.Set<Offer>().Where(o => o.Account.Username == driverUsername).ToList();
            return offerQuery.Select(Mapper.Map<Offer, OfferDto>).AsQueryable();
        }
    }
}