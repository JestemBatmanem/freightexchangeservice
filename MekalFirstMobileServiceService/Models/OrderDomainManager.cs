﻿using System.Linq;
using System.Net.Http;
using AutoMapper;
using MekalFirstMobileServiceService.DataObjects;
using MekalFirstMobileServiceService.DTO;
using Microsoft.WindowsAzure.Mobile.Service;

namespace MekalFirstMobileServiceService.Models
{
    internal class OrderDomainManager : ComplexTypeDomainManager<OrderDto, Order, string>
    {
        public OrderDomainManager(HttpRequestMessage request,
            ApiServices services) : base(new FreightExchangeDbContext(), request, services)
        {
        }

        public IQueryable<OrderDto> GetCustomerOrders(string driverUsername)
        {
            var orderQuery = Context.Set<Order>().Where(o => o.Account.Username == driverUsername).ToList();
            return orderQuery.Select(Mapper.Map<Order, OrderDto>).AsQueryable();
        }
    }
}