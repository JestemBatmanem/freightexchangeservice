﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.OData;
using AutoMapper;
using Microsoft.WindowsAzure.Mobile.Service;
using Microsoft.WindowsAzure.Mobile.Service.Tables;

namespace MekalFirstMobileServiceService.Models
{
    internal class ComplexTypeDomainManager<TData, TModel, TKey> : MappedEntityDomainManager<TData, TModel>
        where TData : class, ITableData
        where TModel : class
    {
        public ComplexTypeDomainManager(DbContext context, HttpRequestMessage request,
            ApiServices services)
            : base(context, request, services)
        {
        }

        public override IQueryable<TData> Query()
        {
            var tModelEnumerable = Context.Set<TModel>().AsEnumerable();
            return Mapper.Map<IEnumerable<TModel>, IEnumerable<TData>>(tModelEnumerable).AsQueryable();
        }

        public override SingleResult<TData> Lookup(string id)
        {
            var tkey = GetKey<TKey>(id);
            TModel model = Context.Set<TModel>().Find(tkey);
            var result = new List<TData>();
            if (model != null)
            {
                result.Add(Mapper.Map<TData>(model));
            }

            return SingleResult.Create(result.AsQueryable());
        }

        public override async Task<TData> UpdateAsync(string id, Delta<TData> patch)
        {
            var tkey = GetKey<TKey>(id);
            TModel model = await Context.Set<TModel>().FindAsync(tkey);
            if (model == null)
            {
                throw new HttpResponseException(Request.CreateNotFoundResponse());
            }

            var data = Mapper.Map<TData>(model);

            patch.Patch(data);
            // Need to update reference types too.
            foreach (string pn in patch.GetChangedPropertyNames())
            {
                Type t;
                if (patch.TryGetPropertyType(pn, out t) && t.IsClass)
                {
                    object v;
                    if (patch.TryGetPropertyValue(pn, out v))
                    {
                        data.GetType().GetProperty(pn).GetSetMethod().Invoke(data, new[] {v});
                    }
                }
            }

            Mapper.Map(data, model);
            await SubmitChangesAsync();

            return data;
        }

        public override Task<bool> DeleteAsync(string id)
        {
            var tkey = GetKey<TKey>(id);
            return DeleteItemAsync(tkey);
        }
    }
}