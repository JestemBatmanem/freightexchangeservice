﻿using System.Data.Entity;
using MekalFirstMobileServiceService.DataObjects;

namespace MekalFirstMobileServiceService.Models
{
    public interface IFreightExchangeDbContext
    {
         IDbSet<Order> Orders { get;  }
         IDbSet<Offer> Offers { get;  }
         IDbSet<DriverData> Drivers { get;  }
         IDbSet<Conversation> Conversations { get;  }
         IDbSet<MeanOfTransport> MeansOfTransport { get;  }
         IDbSet<Account> Accounts { get;}
         int SaveChanges();
    }
}