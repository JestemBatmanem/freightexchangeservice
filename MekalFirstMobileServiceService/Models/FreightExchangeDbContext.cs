﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using MekalFirstMobileServiceService.DataObjects;
using Microsoft.WindowsAzure.Mobile.Service;
using Microsoft.WindowsAzure.Mobile.Service.Tables;

namespace MekalFirstMobileServiceService.Models
{
    public class FreightExchangeDbContext : DbContext, IFreightExchangeDbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to alter your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
        //
        // To enable Entity Framework migrations in the cloud, please ensure that the 
        // service name, set by the 'MS_MobileServiceName' AppSettings in the local 
        // Web.config, is the same as the service name when hosted in Azure.
        private const string connectionStringName = "Name=MS_TableConnectionString";

        public FreightExchangeDbContext() : base(connectionStringName)
        {
        } 

        public IDbSet<Order> Orders { get; set; }
        public IDbSet<Offer> Offers { get; set; }
        public IDbSet<DriverData> Drivers { get; set; }
        public IDbSet<Conversation> Conversations { get; set; }
        public IDbSet<MeanOfTransport> MeansOfTransport { get; set; }
        public IDbSet<Account> Accounts { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            string schema = ServiceSettingsDictionary.GetSchemaName();
            if (!string.IsNullOrEmpty(schema))
            {
                modelBuilder.HasDefaultSchema(schema);
            }

            modelBuilder.Conventions.Add(
                new AttributeToColumnAnnotationConvention<TableColumnAttribute, string>(
                    "ServiceTableColumn", (property, attributes) => attributes.Single().ColumnType.ToString()));
            modelBuilder.Entity<Order>().HasRequired(o => o.Account).WithMany().WillCascadeOnDelete(false);
            modelBuilder.Entity<Offer>().HasRequired(o => o.Account).WithMany().WillCascadeOnDelete(false);
            base.OnModelCreating(modelBuilder);
        }
    }

}
