﻿using System.Collections.Generic;
using MekalFirstMobileServiceService.DataObjects;

namespace MekalFirstMobileServiceService.Models
{
    public interface IConversationManager
    {
        IFreightExchangeDbContext Context { get; }
        Conversation GetOrDefault(string username1, string username2);
        IEnumerable<Conversation> GetAllWithUser(string username);
        void Update(Conversation conversation);
        void SaveChanges();
    }
}
