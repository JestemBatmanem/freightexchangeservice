﻿namespace MekalFirstMobileServiceService.Utils
{
    public enum ServiceRoles
    {
        Customer,
        Driver,
        Admin
    }
}