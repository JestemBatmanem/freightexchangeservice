using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Linq;
using MekalFirstMobileServiceService.DataObjects;

namespace MekalFirstMobileServiceService.Utils
{
    public static class OrderUtils
    {
        public static bool AreOrderCollectionsEqual(IEnumerable<Order> orders1, IEnumerable<Order> orders2)
        {
            return orders1.OrderBy(o => o.Id).SequenceEqual(orders2.OrderBy(o => o.Id));
        }
        //radius in meters!
        public static IEnumerable<Order> QueryClosestOrdersWithinRadius(IQueryable<Order> allOrders, DbGeography location, double radius)
        {
            return (from orders in allOrders
                    where orders.IssueLocation.Distance(location) < radius
                    orderby orders.IssueLocation.Distance(location)
                    select orders).ToList();
        }
    }
}