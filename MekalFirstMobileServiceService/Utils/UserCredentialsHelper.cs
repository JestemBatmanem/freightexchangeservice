﻿using System.Linq;
using System.Web.Http;
using MekalFirstMobileServiceService.Access;
using Microsoft.ServiceBus;
using Microsoft.WindowsAzure.Mobile.Service.Security;

namespace MekalFirstMobileServiceService.Utils
{
    public static class UserCredentialsHelper
    {
        public static string GetCurrentUserName(ApiController controller)
        {
            return GetCurrentUserName(controller.User as ServiceUser);
        }

        public static string GetCurrentUserName(ServiceUser user)
        {
            if (user == null) throw new ServerErrorException("User is null!");
            return user.Id.Split(':')[1];
        }

        public static string CreateFullUsernameWithProvider(string login)
        {
            return CustomLoginProvider.ProviderName + ":" + login;
        }

        public static bool IsControllerUserInRole(ApiController controller, ServiceRoles role)
        {
            var user = controller.User as ServiceUser;
            var userIdentities = user.GetIdentitiesAsync().Result;
            var userRole = (userIdentities.First() as CustomLoginProviderCredentials).Role;
            return userRole == role;
        }
    }
}