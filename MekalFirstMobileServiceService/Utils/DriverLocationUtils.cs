﻿using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Linq;
using MekalFirstMobileServiceService.DataObjects;
using MekalFirstMobileServiceService.Models;

namespace MekalFirstMobileServiceService.Utils
{
    public class DriverLocationUtils
    {
        private const double RadiusForDriverNotification = 20000;
        public IFreightExchangeDbContext Context { get; set; }
        public DriverLocationUtils()
        {
            Context = new FreightExchangeDbContext();
        }

        public IEnumerable<string> GetClosestDriverUsernamesWithinRadius(DbGeography issueLocation)
        {
            return GetClosestDriverUsernamesWithinRadius(Context.Drivers, issueLocation,
                RadiusForDriverNotification);
        }

        public IEnumerable<string> GetClosestDriverUsernamesNearOrder(string orderId)
        {
            var order = Context.Orders.SingleOrDefault(o => o.Id == orderId);
            if (order == default(Order)) return Enumerable.Empty<string>();
            return GetClosestDriverUsernamesWithinRadius(Context.Drivers, order.IssueLocation,
                RadiusForDriverNotification);
        }

        public static IEnumerable<string> GetClosestDriverUsernamesWithinRadius(IQueryable<DriverData> allDrivers, DbGeography location, double radius)
        {
            return (from drivers in allDrivers
                    where drivers.CurrentLocation.Distance(location) < radius
                    select drivers.Account.Username).ToList();
        }
    }
}