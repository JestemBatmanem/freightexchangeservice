﻿using System.Linq;
using System.Text.RegularExpressions;
using MekalFirstMobileServiceService.DTO;

namespace MekalFirstMobileServiceService.Utils
{
    public static class RegistrationRequestValidator
    {
        public const string ValidationSuccessMessage = "OK";
        public const string UsernameAlreadyExistErrorMessage = "Username already exists";
        public const string ErrorGivenUserRoleDoesNotExist = "Error: given user role does not exist";
        public const string InvalidUsernameErrorMessage = "Invalid username (at least 4 chars, alphanumeric only)";
        public const string InvalidPasswordErrorMessage = "Invalid password (at least 8 chars required)";
        public const string InvalidEmailErrorMessage = "Email is invalid";
        public const string EmailAlreadyExistErrorMessage = "There is already an account registered for this email.";

        public static string ValidateRegistrationRequest(RegistrationRequestDto registrationRequest, IQueryable<DataObjects.Account> accounts)
        {
            if (registrationRequest.Role > 1) return ErrorGivenUserRoleDoesNotExist;
            if (!Regex.IsMatch(registrationRequest.Username, "^[a-zA-Z0-9]{4,}$"))
            {
                return InvalidUsernameErrorMessage;
            }
            if (registrationRequest.Password.Length < 8)
            {
                return InvalidPasswordErrorMessage;
            }
            if (!Regex.IsMatch(registrationRequest.Email, @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$"))
            {
                return InvalidEmailErrorMessage;
            }
            DataObjects.Account account = accounts.SingleOrDefault(a => a.Username == registrationRequest.Username || a.Email == registrationRequest.Email);
            if (account != null)
            {
                var usernameOrEmailAlreadyExistErrorMessage = account.Username == registrationRequest.Username ? UsernameAlreadyExistErrorMessage : EmailAlreadyExistErrorMessage;
                return usernameOrEmailAlreadyExistErrorMessage;
            }
            return ValidationSuccessMessage;
        }
    }
}