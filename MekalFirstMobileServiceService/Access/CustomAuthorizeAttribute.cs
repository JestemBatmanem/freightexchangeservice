﻿using System;
using System.Web.Http;
using System.Web.Http.Controllers;
using MekalFirstMobileServiceService.Utils;

namespace MekalFirstMobileServiceService.Access
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
        private ServiceRoles _role;
        public CustomAuthorizeAttribute(ServiceRoles role)
        {
            _role = role;
        }

        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            if (!base.IsAuthorized(actionContext)) return false;
            var controller = actionContext.ControllerContext.Controller as ApiController;
            return UserCredentialsHelper.IsControllerUserInRole(controller, _role) ||
                UserCredentialsHelper.IsControllerUserInRole(controller, ServiceRoles.Admin);
        }
    }
}