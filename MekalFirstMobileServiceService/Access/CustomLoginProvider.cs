﻿using System;
using System.Security.Claims;
using MekalFirstMobileServiceService.Utils;
using Microsoft.WindowsAzure.Mobile.Service;
using Microsoft.WindowsAzure.Mobile.Service.Security;
using Newtonsoft.Json.Linq;
using Owin;

namespace MekalFirstMobileServiceService.Access
{
    public class CustomLoginProvider : LoginProvider
    {
        public const string ProviderName = "Custom";
        public override string Name
        {
            get { return ProviderName; }
        }

        public CustomLoginProvider(IServiceTokenHandler tokenHandler)
            : base(tokenHandler)
        {
            TokenLifetime = new TimeSpan(30, 0, 0, 0);
        }

        public override void ConfigureMiddleware(IAppBuilder appBuilder, ServiceSettingsDictionary settings)
        {
            // Not Applicable - used for federated identity flows
        }

        public override ProviderCredentials CreateCredentials(ClaimsIdentity claimsIdentity)
        {
            if (claimsIdentity == null)
            {
                throw new ArgumentNullException("claimsIdentity");
            }

            string username = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier).Value;
            var credentials = new CustomLoginProviderCredentials
            {
                UserId = TokenHandler.CreateUserId(Name, username),
                Provider = Name,
                Role = (ServiceRoles)Byte.Parse(claimsIdentity.FindFirst(ClaimTypes.Role).Value)
            };

            return credentials;
        }

        public override ProviderCredentials ParseCredentials(JObject serialized)
        {
            if (serialized == null)
            {
                throw new ArgumentNullException("serialized");
            }
            return serialized.ToObject<CustomLoginProviderCredentials>();
        }

    }
}