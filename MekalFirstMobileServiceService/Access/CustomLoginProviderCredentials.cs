﻿using MekalFirstMobileServiceService.Utils;
using Microsoft.WindowsAzure.Mobile.Service.Security;

namespace MekalFirstMobileServiceService.Access
{
    public class CustomLoginProviderCredentials : ProviderCredentials
    {
        public ServiceRoles Role { get; set; }
        public CustomLoginProviderCredentials() : base(CustomLoginProvider.ProviderName)
        {
        }
    }
}