﻿using System;
using System.Collections.Generic;
using Microsoft.WindowsAzure.Mobile.Service;

namespace MekalFirstMobileServiceService.DataObjects
{
    public class Conversation : EntityData
    {
        public string Username1 { get; set; }
        public string Username2 { get; set; }
        public DateTime LastMessageSentTime { get; set; }
        public string LastMessageText { get; set; }
        public virtual ICollection<Message> Messages { get; set; }

        public void UpdateConversationWithNewMessage(string sender, string text)
        {
            var timeSent = DateTime.Now;
            LastMessageSentTime = timeSent;
            LastMessageText = text;
            Messages.Add(new Message
            {
                Id = Guid.NewGuid().ToString(),
                Sender = sender,
                TimeSent = timeSent,
                Text = text
            });
        }
    }
}
