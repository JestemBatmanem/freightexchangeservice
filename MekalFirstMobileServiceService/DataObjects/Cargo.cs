﻿using System.ComponentModel.DataAnnotations;
using Microsoft.WindowsAzure.Mobile.Service;

namespace MekalFirstMobileServiceService.DataObjects
{
    public class Cargo : EntityData
    {
        public float Width { get;  set; }
        public float Length { get;  set; }
        public float Height { get;  set; }
        public float Weight { get;  set; }
        public string Title { get;  set; }
        public string Description { get;  set; }
        [Required]
        public virtual Order Order { get; set; }
    }
}
