﻿using System.ComponentModel.DataAnnotations;
using Microsoft.WindowsAzure.Mobile.Service;

namespace MekalFirstMobileServiceService.DataObjects
{
    public class MeanOfTransport : EntityData
    {
        public float Width { get; set; }
        public float Height { get; set; }
        public float Length { get; set; }
        public int Weight { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        [Required]
        public virtual Account Account { get; set; }
    }
}
