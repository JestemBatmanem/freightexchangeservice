﻿using System.ComponentModel.DataAnnotations;
using Microsoft.WindowsAzure.Mobile.Service;

namespace MekalFirstMobileServiceService.DataObjects
{
    public class Offer : EntityData
    {
        public string Description { get; set; }
        public decimal Value { get; set; }
        public string Currency { get; set; }
        public bool IsAccepted { get; set; }
        [Required]
        public virtual Account Account { get; set; }
        [Required]        
        public virtual Order Order { get; set; }
        public virtual MeanOfTransport MeanOfTransport { get; set; }

        public Offer()
        {
            IsAccepted = false;
        }
    }
}
