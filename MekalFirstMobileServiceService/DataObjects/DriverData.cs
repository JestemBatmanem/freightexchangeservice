﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
using Microsoft.WindowsAzure.Mobile.Service;

namespace MekalFirstMobileServiceService.DataObjects
{
    public class DriverData : EntityData
    {
        public DbGeography CurrentLocation { get; set; }
        [Required]
        public virtual Account Account { get; set; }
    }
}
