﻿using Microsoft.WindowsAzure.Mobile.Service;

namespace MekalFirstMobileServiceService.DataObjects
{
    public class Account : EntityData
    {
        public byte Role { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public byte[] Salt { get; set; }
        public byte[] SaltedAndHashedPassword { get; set; }
    }
}