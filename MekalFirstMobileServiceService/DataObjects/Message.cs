﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.WindowsAzure.Mobile.Service;

namespace MekalFirstMobileServiceService.DataObjects
{
    public class Message : EntityData
    {
        public string Sender { get; set; }
        public string Text { get; set; }
        public DateTime TimeSent { get; set; }
        [Required]        
        public virtual Conversation Conversation { get; set; }
    }
}
