﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Spatial;
using MekalFirstMobileServiceService.ValueObjects;
using Microsoft.WindowsAzure.Mobile.Service;

namespace MekalFirstMobileServiceService.DataObjects
{
    public class Order : EntityData, IEquatable<Order>
    {
        public Address CargoIssueAddress { get; set; }

        public DateTime IssueDate { get; set; }

        public DbGeography IssueLocation { get; set; }

        public Address CargoDeliveryAddress { get; set; }

        public DateTime DeliveryDate { get; set; }

        public DbGeography DeliveryLocation { get; set; }
        [Required]        
        public virtual Account Account { get; set; }

        public virtual ICollection<Cargo> Cargos { get; set; }

        public virtual ICollection<Offer> Offers { get; set; }

        public bool Equals(Order other)
        {
            return !ReferenceEquals(other, null) && Id.Equals(other.Id);
        }
    }
}
