﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MekalFirstMobileServiceService.Access;
using MekalFirstMobileServiceService.Models;
using MekalFirstMobileServiceService.Utils;
using Microsoft.WindowsAzure.Mobile.Service;

namespace MekalFirstMobileServiceService.Controllers
{
    public class AcceptOfferController : ApiController
    {
        public ApiServices Services { get; set; }
        public IFreightExchangeDbContext Context { get; set; }

        [CustomAuthorize(ServiceRoles.Customer)]
        // POST acceptOffer/{id}
        public HttpResponseMessage Post(string id)
        {
            var offerQuery = Context.Offers.Where(o => o.Id == id).ToList();
            if (offerQuery.Count != 1) return Request.CreateResponse(HttpStatusCode.NotFound);
            var offer = offerQuery.Single().IsAccepted = true;
            Context.SaveChanges();
            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}
