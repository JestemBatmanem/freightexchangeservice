﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MekalFirstMobileServiceService.DataObjects;
using MekalFirstMobileServiceService.DTO;
using MekalFirstMobileServiceService.Models;
using MekalFirstMobileServiceService.Utils;
using Microsoft.WindowsAzure.Mobile.Service;
using Microsoft.WindowsAzure.Mobile.Service.Security;

namespace MekalFirstMobileServiceService.Controllers
{
    [AuthorizeLevel(AuthorizationLevel.Anonymous)]
    public class CustomRegistrationController : ApiController
    {
        public ApiServices Services { get; set; }
        public IFreightExchangeDbContext Context { get; set; }

        [Route("register")]
        // POST api/CustomRegistration
        public HttpResponseMessage Post(RegistrationRequestDto registrationRequest)
        {
            var validationMessage = RegistrationRequestValidator.ValidateRegistrationRequest(registrationRequest, Context.Accounts);
            if (validationMessage != RegistrationRequestValidator.ValidationSuccessMessage)
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = validationMessage });
            byte[] salt = HashingUtils.GenerateSalt();
            var newAccount = new Account
            {
                Role = registrationRequest.Role,
                Id = Guid.NewGuid().ToString(),
                Email = registrationRequest.Email,
                Username = registrationRequest.Username,
                Salt = salt,
                SaltedAndHashedPassword = HashingUtils.Hash(registrationRequest.Password, salt)
            };
            Context.Accounts.Add(newAccount);

            if (newAccount.Role == (byte)ServiceRoles.Driver)
            {
                Context.Drivers.Add(new DriverData
                {
                   Id = Guid.NewGuid().ToString(),
                   Account = newAccount
                });
            }
            Context.SaveChanges();
            return Request.CreateResponse(HttpStatusCode.Created);
        }

    }
}
