﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using MekalFirstMobileServiceService.Access;
using MekalFirstMobileServiceService.DTO;
using MekalFirstMobileServiceService.Models;
using MekalFirstMobileServiceService.Utils;
using Microsoft.WindowsAzure.Mobile.Service;
using Microsoft.WindowsAzure.Mobile.Service.Security;

namespace MekalFirstMobileServiceService.Controllers
{
    [AuthorizeLevel(AuthorizationLevel.User)]
    [RoutePrefix("offer")]
    public class OfferController : TableController<OfferDto>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            DomainManager = new OfferDomainManager(Request, Services);
        }

        // GET /Offer
        public IQueryable<OfferDto> GetAllOfferDto()
        {
            return Query();
        }

        // GET /Offer/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<OfferDto> GetOfferDto(string id)
        {
            return Lookup(id);
        }

        [CustomAuthorize(ServiceRoles.Driver)]
        // PATCH /Offer/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<OfferDto> PatchOfferDto(string id, Delta<OfferDto> patch)
        {
            var userOfferList = ((OfferDomainManager)DomainManager).GetDriverOffers(UserCredentialsHelper.GetCurrentUserName(this)).ToList();
            if (userOfferList.Any(o => o.Id == id)) return UpdateAsync(id, patch);
            return Task.FromResult(patch.GetEntity());
        }

        [CustomAuthorize(ServiceRoles.Driver)]
        // POST /Offer
        public async Task<IHttpActionResult> PostOfferDto(OfferDto item)
        {
            OfferDto current = await InsertAsync(item);
            return CreatedAtRoute("Default", new {id = current.Id}, current);
        }

        [CustomAuthorize(ServiceRoles.Driver)]
        // DELETE /Offer/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public async Task<IHttpActionResult> DeleteOfferDto(string id)
        {
            var userOfferList = ((OfferDomainManager)DomainManager).GetDriverOffers(UserCredentialsHelper.GetCurrentUserName(this)).ToList();
            if (userOfferList.All(o => o.Id != id)) return Unauthorized(); 
            await DeleteAsync(id);
            return StatusCode(HttpStatusCode.NoContent);
        }
        [Route("myoffers")]
        [CustomAuthorize(ServiceRoles.Driver)]
        public IQueryable<OfferDto> GetDriverOffers()
        {
            return ((OfferDomainManager) DomainManager).GetDriverOffers(UserCredentialsHelper.GetCurrentUserName(this));
        }
    }
}