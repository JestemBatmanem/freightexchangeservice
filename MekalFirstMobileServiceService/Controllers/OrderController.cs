﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using AutoMapper;
using MekalFirstMobileServiceService.Access;
using MekalFirstMobileServiceService.DataObjects;
using MekalFirstMobileServiceService.DTO;
using MekalFirstMobileServiceService.Models;
using MekalFirstMobileServiceService.Utils;
using Microsoft.WindowsAzure.Mobile.Service;
using Microsoft.WindowsAzure.Mobile.Service.Security;

namespace MekalFirstMobileServiceService.Controllers
{
    [AuthorizeLevel(AuthorizationLevel.User)]
    [RoutePrefix("order")]
    public class OrderController : TableController<OrderDto>
    {
        private DriverLocationUtils _driverLocationUtils;
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            DomainManager = new OrderDomainManager(Request, Services);
            _driverLocationUtils = new DriverLocationUtils();
        }

        // GET /Order
        public IQueryable<OrderDto> GetAllOrderDto()
        {
            return Query();
        }

        // GET /Order/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<OrderDto> GetOrderDto(string id)
        {
            return Lookup(id);
        }

        [CustomAuthorize(ServiceRoles.Customer)]
        // PATCH /Order/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<OrderDto> PatchOrderDto(string id, Delta<OrderDto> patch)
        {
            var userOrderList = ((OrderDomainManager)DomainManager).GetCustomerOrders(UserCredentialsHelper.GetCurrentUserName(this)).ToList();
            if (userOrderList.Any(o => o.Id == id)) return UpdateAsync(id, patch);
            return Task.FromResult(patch.GetEntity());
        }

        [CustomAuthorize(ServiceRoles.Customer)]
        // POST /Order
        public async Task<IHttpActionResult> PostOrderDto(OrderDto item)
        {
            OrderDto current = await InsertAsync(item);
            var driverUsernamesForPush = _driverLocationUtils.GetClosestDriverUsernamesWithinRadius(Mapper.Map<Order>(item).IssueLocation).ToList();
            if (driverUsernamesForPush.Any()) await Services.Push.SendAsync(new ApplePushMessage { { "newOrders", null } }, driverUsernamesForPush);
            return CreatedAtRoute("Default", new { id = current.Id }, current);
        }

        [CustomAuthorize(ServiceRoles.Customer)]
        // DELETE /Order/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public async Task<IHttpActionResult> DeleteOrderDto(string id)
        {
            var userOrderList = ((OrderDomainManager)DomainManager).GetCustomerOrders(UserCredentialsHelper.GetCurrentUserName(this)).ToList();
            if (userOrderList.All(o => o.Id != id)) return Unauthorized();
            var driverUsernamesForPush = _driverLocationUtils.GetClosestDriverUsernamesNearOrder(id).ToList();
            if (driverUsernamesForPush.Any()) await Services.Push.SendAsync(new ApplePushMessage { { "newOrders", null } }, driverUsernamesForPush);
            await DeleteAsync(id);
            return StatusCode(HttpStatusCode.NoContent);
        }

        [Route("myorders")]
        [CustomAuthorize(ServiceRoles.Customer)]
        public IQueryable<OrderDto> GetForCustomer()
        {
            return ((OrderDomainManager)DomainManager).GetCustomerOrders(UserCredentialsHelper.GetCurrentUserName(this));
        }
    }
}