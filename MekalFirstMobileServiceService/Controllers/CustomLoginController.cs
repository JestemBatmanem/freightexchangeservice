﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using MekalFirstMobileServiceService.Access;
using MekalFirstMobileServiceService.DataObjects;
using MekalFirstMobileServiceService.DTO;
using MekalFirstMobileServiceService.Models;
using MekalFirstMobileServiceService.Utils;
using Microsoft.WindowsAzure.Mobile.Service;
using Microsoft.WindowsAzure.Mobile.Service.Security;

namespace MekalFirstMobileServiceService.Controllers
{
    [AuthorizeLevel(AuthorizationLevel.Anonymous)]
    public class CustomLoginController : ApiController
    {
        public IFreightExchangeDbContext Context { get; set; }
        public ApiServices Services { get; set; }
        public IServiceTokenHandler Handler { get; set; }

        //TODO Far future - consider security improvements - brute force detection strategy
        [Route("login")]
        // POST api/CustomLogin
        public HttpResponseMessage Post(LoginRequestDto loginRequest)
        {
            if (!AreCredentialsValid(loginRequest.Username, loginRequest.Password))
                return Request.CreateResponse(HttpStatusCode.Unauthorized, new { Message = "Invalid username or password" });
            var claimsIdentity = new ClaimsIdentity();
            claimsIdentity.AddClaim(new Claim(ClaimTypes.NameIdentifier, loginRequest.Username));
            claimsIdentity.AddClaim(new Claim(ClaimTypes.Role, GetUserRole(loginRequest.Username).ToString()));
            LoginResult loginResult = new CustomLoginProvider(Handler).CreateLoginResult(claimsIdentity,
                Services.Settings.MasterKey);
            return Request.CreateResponse(HttpStatusCode.OK, loginResult);
        }

        private bool AreCredentialsValid(string username, string password)
        {
            Account account = Context.Accounts.SingleOrDefault(a => a.Username == username);
            if (account == null) return false;
            byte[] incoming = HashingUtils.Hash(password, account.Salt);

            return HashingUtils.SlowEquals(incoming, account.SaltedAndHashedPassword);
        }

        private byte GetUserRole(string username)
        {
            return Context.Accounts.Single(a => a.Username == username).Role;
        }
    }
}