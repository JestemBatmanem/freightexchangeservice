﻿using System.Web.Http;
using Microsoft.WindowsAzure.Mobile.Service.Controllers;
using Microsoft.WindowsAzure.Mobile.Service.Security;

namespace MekalFirstMobileServiceService.Controllers.DocumentationControllers
{
    //https://marcominerva.wordpress.com/2014/02/28/authorization-and-public-help-pages-for-mobile-services-written-in-net/
    [AuthorizeLevel(AuthorizationLevel.Anonymous)]
    public class PublicHelpController : HelpController
    {
        [HttpGet]
        [Route("help")]
        public new IHttpActionResult Index()
        {
            return base.Index();
        }

        [HttpGet]
        [Route("help/api/{apiId}")]
        public new IHttpActionResult Api(string apiId)
        {
            return base.Api(apiId);
        }
    }
}