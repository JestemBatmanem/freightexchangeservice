﻿using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.WindowsAzure.Mobile.Service.Controllers;
using Microsoft.WindowsAzure.Mobile.Service.Security;

namespace MekalFirstMobileServiceService.Controllers.DocumentationControllers
{
    [AuthorizeLevel(AuthorizationLevel.Anonymous)]
    public class PublicContentController : ContentController
    {
        [HttpGet]
        [Route("content/{*path}")]
        public new async Task<HttpResponseMessage> Index(string path = null)
        {
            return await base.Index(path);
        }
    }
}