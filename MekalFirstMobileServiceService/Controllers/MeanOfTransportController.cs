﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using AutoMapper;
using MekalFirstMobileServiceService.Access;
using MekalFirstMobileServiceService.DataObjects;
using MekalFirstMobileServiceService.DTO;
using MekalFirstMobileServiceService.Models;
using MekalFirstMobileServiceService.Utils;
using Microsoft.WindowsAzure.Mobile.Service;

namespace MekalFirstMobileServiceService.Controllers
{
    [CustomAuthorize(ServiceRoles.Driver)]
    public class MeanOfTransportController : ApiController
    {
        public ApiServices Services { get; set; }
        public IFreightExchangeDbContext Context { get; set; }

        // GET MeanOfTransport
        public IQueryable<Object> Get()
        {
            string username = UserCredentialsHelper.GetCurrentUserName(this);
            var vehicles = Context.MeansOfTransport.Where(m => m.Account.Username == username);
            return vehicles.Select(v => new{v.Id, v.Name, v.Description, v.Length, v.Width, v.Height, v.Weight}).AsQueryable();
        }

        public HttpResponseMessage Post(MeanOfTransportDto vehicle)
        {
            string username = UserCredentialsHelper.GetCurrentUserName(this);
            var user = Context.Accounts.Single(a => a.Username == username);
            Context.MeansOfTransport.Add(new MeanOfTransport
            {
                Id = Guid.NewGuid().ToString(),
                Description = vehicle.Description,
                Height = vehicle.Height,
                Length = vehicle.Length,
                Name = vehicle.Name,
                Weight = vehicle.Weight,
                Width = vehicle.Width,
                Account = user
            });
            Context.SaveChanges();
            return Request.CreateResponse(HttpStatusCode.NoContent);
        }

        public HttpResponseMessage Delete(string id)
        {
            string username = UserCredentialsHelper.GetCurrentUserName(this);
            var vehicleList = Context.MeansOfTransport.Where(m => m.Account.Username == username).ToList();
            if (vehicleList.Count() == 1)
            {
                Context.MeansOfTransport.Remove(vehicleList.Single());
                Context.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.NoContent);                
            }
            return Request.CreateResponse(HttpStatusCode.NotFound);
        }

    }
}
