﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.OData;
using MekalFirstMobileServiceService.DataObjects;
using MekalFirstMobileServiceService.Models;
using MekalFirstMobileServiceService.Utils;
using Microsoft.WindowsAzure.Mobile.Service;
using Microsoft.WindowsAzure.Mobile.Service.Security;

namespace MekalFirstMobileServiceService.Controllers
{
    //TODO: Consider aync controller
    [AuthorizeLevel(AuthorizationLevel.User)]
    public class ConversationController : ApiController
    {
        public ApiServices Services { get; set; }
        public IConversationManager ConversationManager { get; set; }

        [EnableQuery]
        // GET conversation        
        public IQueryable<Object> Get()
        {
            string username = UserCredentialsHelper.GetCurrentUserName(this);
            return ConversationManager.GetAllWithUser(username)
                .Select(c =>
                {
                    var header = new 
                    {
                        OtherUserInConversation = c.Username1 == username ? c.Username2 : c.Username1,
                        c.LastMessageSentTime,
                        c.LastMessageText
                    };
                    return header;
                }).AsQueryable();
        }

        [EnableQuery]
        [Route("conversation/{user}")]
        // GET conversation/user123
        public IQueryable<Object> Get(string user)
        {
            Conversation conversation = ConversationManager.GetOrDefault(
                UserCredentialsHelper.GetCurrentUserName(this), user);
            return conversation == null
                ? new List<Object>().AsQueryable()
                : conversation.Messages.Select(
                    m => new{m.Sender, m.TimeSent, m.Text}).AsQueryable();
        }
    }
}