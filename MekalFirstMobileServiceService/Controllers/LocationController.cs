﻿using System;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using MekalFirstMobileServiceService.Access;
using MekalFirstMobileServiceService.DTO;
using MekalFirstMobileServiceService.Models;
using MekalFirstMobileServiceService.Utils;
using Microsoft.WindowsAzure.Mobile.Service;

namespace MekalFirstMobileServiceService.Controllers
{
    [CustomAuthorize(ServiceRoles.Driver)]
    public class LocationController : ApiController
    {
        public IFreightExchangeDbContext Context { get; set; }
        public ApiServices Services { get; set; }

        // POST api/Location
        public async Task<HttpResponseMessage> Post(LocationUpdateDto locationUpdate)
        {
            string driverName = UserCredentialsHelper.GetCurrentUserName(ControllerContext.Controller as ApiController);

            DbGeography formerLocation = Context.Drivers.Single(d => d.Account.Username == driverName).CurrentLocation;
            DbGeography currentLocation =
                DbGeography.FromText(String.Format("POINT({0} {1})", locationUpdate.Latitude, locationUpdate.Longitude));

            bool sendNotification =
                !OrderUtils.AreOrderCollectionsEqual(
                    OrderUtils.QueryClosestOrdersWithinRadius(Context.Orders, formerLocation, locationUpdate.Radius),
                    OrderUtils.QueryClosestOrdersWithinRadius(Context.Orders, currentLocation, locationUpdate.Radius));

            Context.Drivers.Single(d => d.Account.Username == driverName).CurrentLocation = currentLocation;
            Context.SaveChanges();
            if (sendNotification) await Services.Push.SendAsync(new ApplePushMessage { { "newOrders", null } }, driverName);
            return Request.CreateResponse(HttpStatusCode.NoContent);
        }
    }
}