﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using MekalFirstMobileServiceService.DTO;
using MekalFirstMobileServiceService.Utils;
using Microsoft.ServiceBus.Notifications;
using Microsoft.WindowsAzure.Mobile.Service.Security;

namespace MekalFirstMobileServiceService.Controllers
{
    [AuthorizeLevel(AuthorizationLevel.User)]
    public class PushNotificationRegistrationController : ApiController
    {
        private readonly NotificationHubClient _hub;
        private readonly Dictionary<string, Action<string, string>> _platformHandlers;

        public PushNotificationRegistrationController()
        {
            _hub =
                NotificationHubClient.CreateClientFromConnectionString(
                    WebConfigurationManager.AppSettings["MS_NotificationHubConnectionString"],
                    WebConfigurationManager.AppSettings["MS_NotificationHubName"]);
            _platformHandlers = new Dictionary<string, Action<string, string>>
            {
                {"mpns", (token, tag) => _hub.CreateMpnsNativeRegistrationAsync(token, new[] {tag})},
                {"apns", (token, tag) => _hub.CreateAppleNativeRegistrationAsync(token, new[] {tag})},
                {"wns", (token, tag) => _hub.CreateWindowsNativeRegistrationAsync(token, new[] {tag})},
                {"gcm", (token, tag) => _hub.CreateGcmNativeRegistrationAsync(token, new[] {tag})}
            };
        }

        [Route("registerPush")]
        // POST api/registerPush
        public HttpResponseMessage Post(RegisterPushDto registerPushDto)
        {
            if (!_platformHandlers.ContainsKey(registerPushDto.Vendor))
                return Request.CreateResponse(HttpStatusCode.BadRequest,
                    new {Message = "Invalid vendor. Should be: apns, mpns, wns or gcm."});
            _platformHandlers[registerPushDto.Vendor].Invoke(registerPushDto.Token, UserCredentialsHelper.GetCurrentUserName(ControllerContext.Controller as ApiController));
            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}