﻿using System;
using System.Collections.Generic;
using System.Linq;
using MekalFirstMobileServiceService.DataObjects;
using MekalFirstMobileServiceService.Models;

namespace MekalFirstMobileServiceService.Chat
{
    public class Chat : IDisposable
    {
        private readonly IConversationManager _conversationManager;
        private readonly Func<Conversation, string, string, bool> _conversationSelectionPredicate;
        public List<Conversation> ActiveConversations { get; private set; }
        public SortedSet<string> ActiveUsers { get; set; }
 
        public Chat(IConversationManager manager)
        {
            ActiveConversations = new List<Conversation>();
            ActiveUsers = new SortedSet<string>();
            _conversationSelectionPredicate =
                (c, username1, username2) => (c.Username1.Equals(username1) && c.Username2.Equals(username2)) ||
                                             (c.Username1.Equals(username2) && c.Username2.Equals(username1));
            _conversationManager = manager;
        }

        ~Chat()
        {
            Dispose();
        }

        public void Dispose()
        {
            foreach (Conversation conversation in ActiveConversations)
            {
                _conversationManager.Update(conversation);
            }
            _conversationManager.SaveChanges();
        } 

        public void SendMessage(string sender, string receiver, string text)
        {
            Conversation conversation = GetFromActiveConversations(sender, receiver);
            if (conversation != null)
            {
                conversation.UpdateConversationWithNewMessage(sender, text);
                return;
            }
            conversation = GetFromArchivedConversations(sender, receiver);
            if (conversation != null)
            {
                conversation.UpdateConversationWithNewMessage(sender, text);
                ActiveConversations.Add(conversation);
            }
            else
            {
                ActiveConversations.Add(CreateConversationWithFirstMessage(sender, receiver, text));
            }
        }

        public void LogIn(string username)
        {
            ActiveUsers.Add(username);            
        }

        public void LogOut(string username)
        {
            ActiveUsers.Remove(username);
            //Archive and remove conversation if both users are not active
            ArchiveAndRemoveFromActiveConversations(
                ActiveConversations.Where(
                    c =>
                        (c.Username1 == username && !ActiveUsers.Contains(c.Username2)) ||
                        (c.Username2 == username && !ActiveUsers.Contains(c.Username1))).ToList());
        }

        private Conversation GetFromActiveConversations(string sender, string receiver)
        {
            return ActiveConversations.SingleOrDefault(c => _conversationSelectionPredicate(c, sender, receiver));
        }

        private Conversation GetFromArchivedConversations(string sender, string receiver)
        {
            return _conversationManager.GetOrDefault(sender, receiver);
        }

        private void ArchiveAndRemoveFromActiveConversations(IEnumerable<Conversation> conversations)
        {
            ActiveConversations.RemoveAll(c => conversations.Any(s => s.Id == c.Id));
            foreach(var c in conversations)_conversationManager.Update(c);
            _conversationManager.SaveChanges();
        }

        private Conversation CreateConversationWithFirstMessage(string sender, string receiver, string text)
        {
            DateTime timeSent = DateTime.Now;
            return new Conversation
            {
                Id = Guid.NewGuid().ToString(),
                LastMessageSentTime = timeSent,
                LastMessageText = text,
                Username1 = sender,
                Username2 = receiver,
                Messages = new List<Message>
                {
                    new Message
                    {
                        Id = Guid.NewGuid().ToString(),
                        Sender = sender,
                        TimeSent = timeSent,
                        Text = text
                    }
                }
            };
        }
    }
}