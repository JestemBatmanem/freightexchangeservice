﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MekalFirstMobileServiceService.Utils;
using Microsoft.AspNet.SignalR;
using Microsoft.WindowsAzure.Mobile.Service;
using Microsoft.WindowsAzure.Mobile.Service.Security;

namespace MekalFirstMobileServiceService.Chat
{
    [AuthorizeLevel(AuthorizationLevel.User)]
    public class ChatHub : Hub
    {
        private readonly Chat _chat;

        public ChatHub(Chat chat)
        {
            _chat = chat;
        }

        public ApiServices Services { get; set; }

        public override Task OnConnected()
        {
            string callerNameWithoutProvider = UserCredentialsHelper.GetCurrentUserName((ServiceUser) Context.User);
            _chat.LogIn(callerNameWithoutProvider);
            return base.OnConnected();
        }

        public async void SendMessageTo(string targetUsername, string message)
        {
            string callerNameWithoutProvider = UserCredentialsHelper.GetCurrentUserName((ServiceUser) Context.User);
            _chat.SendMessage(callerNameWithoutProvider, targetUsername, message);
            if (_chat.ActiveUsers.Contains(targetUsername))
                Clients.User(UserCredentialsHelper.CreateFullUsernameWithProvider(targetUsername))
                    .getMessage(callerNameWithoutProvider, message);
            else
            {
                await
                    Services.Push.SendAsync(
                        new ApplePushMessage
                        {
                            {
                                "message",
                                new Dictionary<string, string>
                                {
                                    {"from", callerNameWithoutProvider},
                                    {"text", message},
                                    {"date", DateTime.Now.ToString()}
                                }
                            }
                        }, targetUsername);
            }
        }

        //TODO: set a timeout if its possible, if he reconnects then don't do this 
        public override Task OnDisconnected(bool stopCalled)
        {
            string callerNameWithoutProvider = UserCredentialsHelper.GetCurrentUserName((ServiceUser) Context.User);
            _chat.LogOut(callerNameWithoutProvider);
            return base.OnDisconnected(stopCalled);
        }
    }
}