﻿using System;
using Microsoft.AspNet.SignalR;
using Microsoft.WindowsAzure.Mobile.Service.Security;

namespace MekalFirstMobileServiceService.Chat
{
    public class CustomUserIdProvider : IUserIdProvider
    {
        public string GetUserId(IRequest request)
        {
            var user = request.User as ServiceUser;
            if (user == null) throw new ArgumentNullException("request");
            return user.Id; 
        }
    }
}