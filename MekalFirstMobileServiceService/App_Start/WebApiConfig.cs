﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Data.Entity.Spatial;
using System.Web.Http;
using Autofac;
using AutoMapper;
using MekalFirstMobileServiceService.Access;
using MekalFirstMobileServiceService.Chat;
using MekalFirstMobileServiceService.DataObjects;
using MekalFirstMobileServiceService.DTO;
using MekalFirstMobileServiceService.Models;
using MekalFirstMobileServiceService.Utils;
using MekalFirstMobileServiceService.ValueObjects;
using Microsoft.AspNet.SignalR;
using Microsoft.WindowsAzure.Mobile.Service;
using Microsoft.WindowsAzure.Mobile.Service.Config;

namespace MekalFirstMobileServiceService
{
    public static class WebApiConfig
    {
        public static void Register()
        {
            // Use this class to set configuration options for your mobile service
            var options = new ConfigOptions();
            options.LoginProviders.Add(typeof (CustomLoginProvider));
            // DI container settings
            var builder = new ConfigBuilder(options,
                (httpConfig, autofac) =>
                {
                    autofac.RegisterInstance(new FreightExchangeDbContext()).As<IFreightExchangeDbContext>();
                    autofac.RegisterInstance(new FreightExchangeDbContext()).As<DbContext>();
                    autofac.RegisterType<ConversationManager>().As<IConversationManager>();
                    autofac.RegisterType<Chat.Chat>().As<Chat.Chat>().InstancePerLifetimeScope();
                    autofac.RegisterType<CustomUserIdProvider>().As<IUserIdProvider>();

                    //TODO: Make autofac work for custom types
                    //autofac.RegisterType<DriverLocationUtils>().PropertiesAutowired();
                    //autofac.RegisterType<DriverLocationUtils>().As<DriverLocationUtils>();
                });

            // Use this class to set WebAPI configuration options
            HttpConfiguration config = ServiceConfig.Initialize(builder);

            SignalRExtensionConfig.Initialize();

            //Treat localhost as hosted environment if true
            config.SetIsHosted(true);

            // To display errors in the browser during development, uncomment the following
            // line. Comment it out again when you deploy your service for production use.
            config.IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.Always;

            config.Routes.Remove("DefaultApis");
            config.Routes.Remove("Tables");
            config.Routes.MapHttpRoute("Default", "{controller}/{id}", new {id = RouteParameter.Optional}
                );

            Database.SetInitializer(new MekalFirstMobileServiceInitializer());

            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<OrderDto, Order>().AfterMap((dto, o) =>
                {
                    o.IssueLocation =
                        DbGeography.FromText(String.Format("POINT({0} {1})", dto.IssueLocationLatitude,
                            dto.IssueLocationLongitude));
                    o.CargoIssueAddress = new Address
                    {
                        City = dto.CargoIssueAddressCity,
                        Street = dto.CargoIssueAddressStreet,
                        StreetNumber = dto.CargoIssueAddressStreetNumber,
                        ApartmentNumber = dto.CargoIssueAddressApartmentNumber
                    };
                    o.CargoDeliveryAddress = new Address
                    {
                        City = dto.CargoDeliveryAddressCity,
                        Street = dto.CargoDeliveryAddressStreet,
                        StreetNumber = dto.CargoDeliveryAddressStreetNumber,
                        ApartmentNumber = dto.CargoDeliveryAddressApartmentNumber
                    };
                });

                cfg.CreateMap<Order, OrderDto>()
                    .ForMember(dto => dto.IssueLocationLatitude, map => map.MapFrom(o => o.IssueLocation.Latitude))
                    .ForMember(dto => dto.IssueLocationLongitude, map => map.MapFrom(o => o.IssueLocation.Longitude));
                cfg.CreateMap<OfferDto, Offer>();
                cfg.CreateMap<Offer, OfferDto>();
            });
        }
    }

    public class MekalFirstMobileServiceInitializer : ClearDatabaseSchemaAlways<FreightExchangeDbContext>
    {
        protected override void Seed(FreightExchangeDbContext context)
        {
            var driverAccounts = new List<Account>();
            var driverDatas = new List<DriverData>();
            var customerAccounts = new List<Account>();
            var conversations = new List<Conversation>();
            var orders = new List<Order>();

            #region seed accounts and driver datas

            byte[] salt = HashingUtils.GenerateSalt();
            driverAccounts.Add(new Account
            {
                Id = Guid.NewGuid().ToString(),
                Role = 1,
                Email = "piotrek2026@hotmail.com",
                Username = "driver",
                Salt = salt,
                SaltedAndHashedPassword = HashingUtils.Hash("piotrekpiotrek", salt)
            });
            salt = HashingUtils.GenerateSalt();
            driverAccounts.Add(new Account
            {
                Id = Guid.NewGuid().ToString(),
                Role = 1,
                Email = "driver1@aaa.com",
                Username = "driver1",
                Salt = salt,
                SaltedAndHashedPassword = HashingUtils.Hash("piotrekpiotrek", salt)
            });

            driverDatas.Add(new DriverData
            {
                Id = Guid.NewGuid().ToString(),
                Account = driverAccounts[0],
                CurrentLocation = DbGeography.FromText("POINT(-120.336106 37.605049)")
            });

            driverDatas.Add(new DriverData
            {
                Id = Guid.NewGuid().ToString(),
                Account = driverAccounts[1],
                CurrentLocation = DbGeography.FromText("POINT(10.336106 2.605049)")
            });

            salt = HashingUtils.GenerateSalt();
            customerAccounts.Add(new Account
            {
                Id = Guid.NewGuid().ToString(),
                Role = 0,
                Email = "mekalpiotr@gmail.com",
                Username = "customer",
                Salt = salt,
                SaltedAndHashedPassword = HashingUtils.Hash("piotrekpiotrek", salt)
            });
            salt = HashingUtils.GenerateSalt();
            customerAccounts.Add(new Account
            {
                Id = Guid.NewGuid().ToString(),
                Role = 0,
                Email = "customer1@gmail.com",
                Username = "customer1",
                Salt = salt,
                SaltedAndHashedPassword = HashingUtils.Hash("piotrekpiotrek", salt)
            });
            foreach (Account a in driverAccounts) context.Accounts.Add(a);
            foreach (Account a in customerAccounts) context.Accounts.Add(a);
            foreach (DriverData d in driverDatas) context.Drivers.Add(d);

            #endregion

            #region seed conversations and messages

            conversations.Add(new Conversation
            {
                Id = Guid.NewGuid().ToString(),
                Username1 = customerAccounts[0].Username,
                Username2 = driverAccounts[0].Username,
                LastMessageSentTime = new DateTime(2015, 6, 6),
                LastMessageText = "Bye!",
            });
            conversations[0].Messages = new List<Message>
            {
                new Message
                {
                    Id = Guid.NewGuid().ToString(),
                    Sender = customerAccounts[0].Username,
                    TimeSent = new DateTime(2015, 6, 4),
                    Text = "Hello!",
                    Conversation = conversations[0]
                },
                new Message
                {
                    Id = Guid.NewGuid().ToString(),
                    Sender = driverAccounts[0].Username,
                    TimeSent = new DateTime(2015, 6, 5),
                    Text = "Hi, how are you?",
                    Conversation = conversations[0]
                },
                new Message
                {
                    Id = Guid.NewGuid().ToString(),
                    Sender = customerAccounts[0].Username,
                    TimeSent = new DateTime(2015, 6, 6),
                    Text = "Got to go. Bye!",
                    Conversation = conversations[0]
                },
            };

            conversations.Add(new Conversation
            {
                Id = Guid.NewGuid().ToString(),
                Username1 = customerAccounts[0].Username,
                Username2 = customerAccounts[1].Username,
                LastMessageSentTime = new DateTime(2015, 7, 7),
                LastMessageText = "Bye",
            });
            conversations[1].Messages = new List<Message>
            {
                new Message
                {
                    Id = Guid.NewGuid().ToString(),
                    Sender = customerAccounts[0].Username,
                    TimeSent = new DateTime(2015, 7, 4),
                    Text = "Hello another customer!",
                    Conversation = conversations[1]
                },
                new Message
                {
                    Id = Guid.NewGuid().ToString(),
                    Sender = customerAccounts[1].Username,
                    TimeSent = new DateTime(2015, 7, 5),
                    Text = "Hi, how are you?",
                    Conversation = conversations[1]
                },
                new Message
                {
                    Id = Guid.NewGuid().ToString(),
                    Sender = customerAccounts[0].Username,
                    TimeSent = new DateTime(2015, 7, 6),
                    Text = "I'm fine.You?",
                    Conversation = conversations[1]
                },
                new Message
                {
                    Id = Guid.NewGuid().ToString(),
                    Sender = customerAccounts[0].Username,
                    TimeSent = new DateTime(2015, 7, 6),
                    Text = "Bye!",
                    Conversation = conversations[1]
                },
            };

            foreach (Conversation c in conversations) context.Conversations.Add(c);

            #endregion

            #region seed orders

            orders.Add(new Order
            {
                Id = Guid.NewGuid().ToString(),
                CargoDeliveryAddress = new Address
                {
                    City = "Kraków"
                },
                CargoIssueAddress = new Address
                {
                    City = "Warszawa"
                },
                IssueLocation = DbGeography.FromText("POINT(-122.336106 47.605049)"),
                IssueDate = new DateTime(2015, 5, 10),
                DeliveryLocation = DbGeography.FromText("POINT(-120.336106 47.605049)"),
                DeliveryDate = new DateTime(2015, 6, 10),
                Account = customerAccounts[0],
                Cargos = new Collection<Cargo>
                {
                    new Cargo
                    {
                        Id = Guid.NewGuid().ToString(),
                        Title = "Fortepian",
                        Width = 200,
                        Height = 200,
                        Length = 150,
                        Weight = 80
                    }
                },
            });

            orders.Add(new Order
            {
                Id = Guid.NewGuid().ToString(),
                CargoDeliveryAddress = new Address
                {
                    City = "Wrocław"
                },
                CargoIssueAddress = new Address
                {
                    City = "Sandomierz"
                },
                IssueLocation = DbGeography.FromText("POINT(-120.120 44.45)"),
                IssueDate = new DateTime(2015, 7, 10),
                DeliveryLocation = DbGeography.FromText("POINT(-125.3 47.0)"),
                DeliveryDate = new DateTime(2015, 7, 10),
                Account = customerAccounts[0],
                Cargos = new Collection<Cargo>
                {
                    new Cargo
                    {
                        Id = Guid.NewGuid().ToString(),
                        Title = "Skrzynia",
                        Width = 100,
                        Height = 100,
                        Length = 110,
                        Weight = 85
                    },
                    new Cargo
                    {
                        Id = Guid.NewGuid().ToString(),
                        Title = "Beczka",
                        Width = 20,
                        Height = 100,
                        Length = 30,
                        Weight = 100
                    },
                    new Cargo
                    {
                        Id = Guid.NewGuid().ToString(),
                        Title = "Paczka",
                        Width = 20,
                        Height = 20,
                        Length = 20,
                        Weight = 20
                    }
                },
            });

            foreach (Order o in orders) context.Orders.Add(o);

            #endregion

            #region seed offers and means of transport

            context.Offers.Add(
                new Offer
                {
                    Id = Guid.NewGuid().ToString(),
                    Currency = "PLN",
                    Description = "Brak",
                    Value = 150,
                    MeanOfTransport = new MeanOfTransport
                    {
                        Id = Guid.NewGuid().ToString(),
                        Description = "Duży fiat",
                        Height = 210,
                        Length = 200,
                        Width = 250,
                        Weight = 300,
                        Name = "Fiat gonzo",
                        Account = driverAccounts[0]
                    },
                    Order = orders[0],
                    Account = driverAccounts[0]
                });
            context.Offers.Add(
                new Offer
                {
                    Id = Guid.NewGuid().ToString(),
                    Currency = "PLN",
                    Description = "Brak",
                    Value = 200,
                    MeanOfTransport = new MeanOfTransport
                    {
                        Id = Guid.NewGuid().ToString(),
                        Description = "Duży ford",
                        Height = 200,
                        Length = 200,
                        Width = 270,
                        Weight = 300,
                        Name = "Ford mondeo kombi",
                        Account = driverAccounts[1]
                    },
                    Order = orders[0],
                    Account = driverAccounts[1]
                });
            context.Offers.Add(
                new Offer
                {
                    Id = Guid.NewGuid().ToString(),
                    Currency = "EUR",
                    Description = "Akurat mam dużo euro",
                    Value = 70,
                    MeanOfTransport = new MeanOfTransport
                    {
                        Id = Guid.NewGuid().ToString(),
                        Description = "Polonez",
                        Height = 200,
                        Length = 200,
                        Width = 270,
                        Weight = 300,
                        Name = "Stary ale stabilny",
                        Account = driverAccounts[0]
                    },
                    Order = orders[1],
                    Account = driverAccounts[0]
                });

            #endregion

            context.SaveChanges();
            base.Seed(context);
        }
    }
}